/*
RAM Assertions
1. 	Read and write enable are mutually exclusive.
	This case is irrelevant for us, because there is only
	one signal that governs read and write.
	
2. 	Address never has X or Z values.

3. 	Data never has any X or Z values.

4.	Checking that write enable should be active for only 1 clock cycle.

5.	Checking that ready bit is asserted on the rising edge of clock right after en=2'b10.

6. 	Checking that valid bit is asserted on the rising edge of clock right after en=2'b01.

7. Checking that ready get de-asserted when reset is there or reset is de-asserted.

8. Read and Write only when Reset is low.

9.  To check proper working of reset - after writing
	to few locations generate reset and check whether 
	default values are getting read
*/

module ram_assertions();	
  /*
	1. Read and write enable are mutually exclusive.
	   this case is irrelevant for us, because there is only
	   one signal that governs read and write.
  */
		
  /*
	2. Address never has X or Z values.
			
  */
  always@(ram_vif.address)
  begin  
    a_no_address_X: assert ($isunknown(ram_vif.address) == 0) begin
     // $display("Assertion 2 Check Successful");
    end
      
	else begin
      $error("address is X or Z");
    end
  end 
  /*
	3. Data never has any X or Z values.
  */
  always@(ram_vif.din)
    begin 
      a_no_data_X	  : assert ($isunknown(ram_vif.din) == 0) begin
        //$display("Assertion 3 Check Successful");
      end
	  else begin
        $error ("data is X or Z");
      end
    end

		
   /*
 	4. Checking that write enable or read enable should be active for only 1 clock cycle.
   */
   //$rose() returns true, when leas significant bit changes to 1
   // $ can be used to extend the window, but unbounded range
   // ##0 has sequential inference		
   property p_en_active_1_cycle;
     @(posedge ram_vif.clk)
     ($rose(ram_vif.en == 2'b01 || ram_vif.en == 2'b10)) |=> ($past(ram_vif.en) != ram_vif.en);
   endproperty:p_en_active_1_cycle
   a_en_active_1_cycle: assert property (p_en_active_1_cycle) begin
    //$display("Assertion 4 Check Successful");
  end
  else begin
     $error("Error: write or read enable was high for multiple clock cycle");
  end
          
  /*
	5. Checking that ready bit is asserted on the rising edge of clock right after en=2'b10.
  */
				
  property p_ready_asserted_after_ren;
    @ (posedge ram_vif.clk)
    (ram_vif.en == 2'b10) |-> ##1 (ram_vif.ready == 1);//1 clock cycle after read enable is there.
  endproperty:p_ready_asserted_after_ren
  a_ready_asserted_after_ren: assert property (p_ready_asserted_after_ren) //$display("Assertion 5 Check Successful");
  else
	$display("Error: Ready pin is not set exactly after read signal, en=2'b10");
 
   /*
	6. Checking that valid bit is asserted on the rising edge of clock right after en=2'b01.
   */
		
   property p_valid_asserted_after_wen;
     @ (posedge ram_vif.clk)
     (ram_vif.en == 2'b01) |-> ##1 (ram_vif.valid == 1);//1 clock cycle after write enable is there.
   endproperty:p_valid_asserted_after_wen
   a_valid_asserted_after_wen: assert property (p_valid_asserted_after_wen) begin
      //$display("Assertion 6 check successful");
   end
   else begin
     $display("Assertion 6 Error: Valid pin is not set exactly after write signal, en=2'b01");
   end
          
   /*
	7. Checking that ready get de-asserted when reset is there.
   */
		
   property p_ready_deasserted_on_reset;
	 @(posedge ram_vif.clk)
     (ram_vif.n_rst == 0) |-> (ram_vif.ready == 0);//1 clock cycle after write enable is there.
   endproperty:p_ready_deasserted_on_reset
   a_ready_deasserted_on_reset: assert property (p_ready_deasserted_on_reset) begin
      // $display("'Assertion 7 Successful': check successful");
   end
   else begin
      $display("Assertion 7 Error: Ready pin getting high when reset is there.");
   end
          
          
  	/*
      8. Read and Write only when Reset is low.
	*/
	property check_readwrite_only_at_reset_low;
	  @(posedge ram_vif.clk)
      //correct code
      (ram_vif.n_rst == 0) |-> ##5(ram_vif.en == 2'b01 || ram_vif.en == 2'b10);
    endproperty:check_readwrite_only_at_reset_low
    a_check_readwrite_only_at_reset_low: assert property (check_readwrite_only_at_reset_low) begin
      //$display("'Assertion 8 Successful': Reset Check for Correct timing of Read Write is Successful.");
    end
	else begin
      $display("Assertion 8 Error: Read Write operation is happening when reset is high");
    end
          
    /*
	  9.  To check proper working of reset - after writing
		  to few locations generate reset and check whether 
		  default values are getting read
	*/
	property check_proper_resetting;
	  @(posedge ram_vif.clk)
      (ram_vif.n_rst == 0) |-> (ram_vif.dout == 0);
	endproperty:check_proper_resetting
    a_check_proper_resetting: assert property (check_proper_resetting) begin
      // $display("Assertion 9 Check Successful");
    end
    else $display("Assertion 9 Error: Reset Not working properly, because after reset dout is not zero");
endmodule