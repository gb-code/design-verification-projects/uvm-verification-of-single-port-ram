class ram_transaction extends uvm_sequence_item;
  //----------------------------------------------------------------------------
  //All Random Varialbes
  //----------------------------------------------------------------------------
  rand bit[`ADDR_WIDTH-1:0] address;
  rand bit[`DATA_WIDTH-1:0] data;
  rand bit[1:0]  en;
  rand bit       valid;
  rand bit[3:0]  mask;
  rand bit       ready;
  rand bit[1:0]  no_op;

  //----------------------------------------------------------------------------
  //Constraints
  //----------------------------------------------------------------------------

  //Generate Valid for Write Requests
  constraint vaild_constraint_c {
    (en == 2'b01) -> (valid == 1);
    (en == 2'b10) -> (valid == 0);
  }
 
  //Generate Only Read/Write Requests - 
  //Make it soft constraint to override later to drive not valid requests
  //to not generate write/read requests from sequence
  constraint en_c      { soft en   inside {[2'b01:2'b10]};}
  constraint no_op_c   { no_op     inside {2'b00,2'b11};}

  //Make it soft constraint later to drive byte_en = 0
  constraint byte_en_c { soft mask inside {[4'b0001:4'b1111]};}

  //----------------------------------------------------------------------------
  `uvm_object_utils_begin(ram_transaction)
    `uvm_field_int(address, UVM_ALL_ON);
    `uvm_field_int(data,    UVM_ALL_ON);
    `uvm_field_int(en,      UVM_ALL_ON);
    `uvm_field_int(valid,   UVM_ALL_ON);
    `uvm_field_int(mask,    UVM_ALL_ON);
    `uvm_field_int(ready,   UVM_ALL_ON);
    `uvm_field_int(no_op,   UVM_ALL_ON);
    `uvm_object_utils_end


  function new(string name = "ram_transaction");
    super.new(name);
  endfunction : new

endclass : ram_transaction
