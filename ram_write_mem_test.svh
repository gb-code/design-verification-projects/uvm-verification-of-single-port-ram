class ram_write_mem_test extends ram_base_test;
  `uvm_component_utils( ram_write_mem_test )
   
  function new( string name="ram_write_mem_test", uvm_component parent );
    super.new( name, parent );
  endfunction: new
 
  function void build_phase( uvm_phase phase );
     super.build_phase( phase );   
  endfunction: build_phase

  virtual function void end_of_elaboration_phase(uvm_phase phase);
    super.end_of_elaboration_phase(phase);
    ram_base_sequence::type_id::set_type_override(ram_write_mem_sequence::get_type());
    
  endfunction : end_of_elaboration_phase  

endclass: ram_write_mem_test
