`include "ram_defines.svh"
interface ram_if(input logic clk,input logic n_rst);

  logic [`ADDR_WIDTH     - 1: 0] address;
  logic [`DATA_WIDTH     - 1: 0] din;
  logic [`DATA_WIDTH     - 1: 0] dout;
  
  logic [1:0] en;
  logic [3:0] mask;
  
  logic valid;
  logic ready;
  logic reset_drive;
  logic generate_reset;
  logic reset;
  
  clocking ram_drv_cb @ (posedge clk);
     default input #1ns output #1ns;
     output din, address, en, mask, valid;
     input  ready, dout;
  endclocking: ram_drv_cb
 
  clocking ram_mon_cb @ (negedge clk);
     default input #1ns output #1ns;
     output din, address, en, mask, valid;
     input  ready, dout;
  endclocking: ram_mon_cb
 
 
  modport ram_mp(input clk, n_rst, ready, dout, output address, din, en, mask, valid);
endinterface
