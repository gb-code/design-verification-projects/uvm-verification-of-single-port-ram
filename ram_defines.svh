typedef enum bit [1:0] {NO_OP1,WRITE,READ,NO_OP2} en_e; 

`ifdef RAM_64K
  `define ADDR_WIDTH 32
`else
  `define ADDR_WIDTH 15
`endif
`define DATA_WIDTH 32

`define RAM_SIZE  (1 << `ADDR_WIDTH)


