// ----------------------------------------------------------------------------
// Description   	: This module implements the single port RAM.
// Bits Calculation	: maximum is 4K bytes => i.e. 4096*8 bits. so 2^12 *2^3 (15 bits) 
// ----------------------------------------------------------------------------
module ram #(parameter ADDRW=15,WIDTH    =32) ( ram_if ram_vif);

parameter DEPTH = (1 << ADDRW);

//=================================================
//Variable Declaration
//================================================
logic                  	 sys_clk;
logic                  	 n_rst;
logic                  	 valid;
logic [ADDRW-1:0]        addr;
logic [1:0]              en;
logic [WIDTH-1:0]        din;
logic [3:0]              byte_en;
logic                    ready;
logic   [WIDTH-1:0]      dout; 
  

  
reg [WIDTH - 1:0]     ram_reg [0:DEPTH-1] ;
reg [WIDTH - 1:0]     data_o;
reg [1:0]             en_in;
reg [ADDRW - 1:0]     addr_in;
reg                   ready_o;
reg [3:0]             rd_wr_valid;

//=================================================
//Interface assignment
//================================================  
assign sys_clk = ram_vif.clk;
assign n_rst   = ram_vif.n_rst;  
assign valid   = ram_vif.valid;
assign ram_vif.ready = ready;
assign ram_vif.dout  = dout;
assign addr    = ram_vif.address;
assign en    = ram_vif.en;
assign din   = ram_vif.din;
assign byte_en = ram_vif.mask;
 
  
always @(posedge sys_clk or negedge n_rst)
begin
  if(n_rst)
  begin
    dout <= data_o;
    ready <= ready_o;
  end
  else
  begin
    ready <= 0;
    ready_o <= 0;
  end
end

always @(negedge sys_clk )
begin
  en_in  <= en;
end

always @* 
begin
  case(en_in)
  2'b01:  begin //write operation
            if(valid)
            begin
              ram_reg[addr] = din & (byte_mask(byte_en, addr));
            end
            ready_o = 0;
          end
  2'b10:  begin//read operation
  	    	data_o  = ram_reg[addr];
            ready_o = 1;
          end
  default:begin
            ready_o = 0;
          end
  endcase
end

function [31:0] byte_mask;
input [3:0]byte_en;
input [ADDRW-1:0] addr;
begin
  if((addr[1:0] == 0))
  begin
    rd_wr_valid = 'hF;
  end
  if((addr[1:0] == 1))
  begin
    rd_wr_valid = 'h7;
  end
  if((addr[1:0] == 2))
  begin
    rd_wr_valid = 'h3;
  end
  if((addr[1:0] == 3))
  begin
    rd_wr_valid = 'h1;
  end

  if((byte_en[0] == 1) && rd_wr_valid[3])
    byte_mask[7:0] = 8'hFF;
  else
    byte_mask[7:0] = 8'h0;

  if((byte_en[1] == 1) && rd_wr_valid[2])
    byte_mask[15:8] = 8'hFF;
  else
    byte_mask[15:8] = 8'h0;

  if((byte_en[2] == 1) && rd_wr_valid[1])
    byte_mask[23:16] = 8'hFF;
  else
    byte_mask[23:16] = 8'h0;

  if((byte_en[3] == 1) && rd_wr_valid[0])
    byte_mask[31:24] = 8'hFF;
  else
    byte_mask[31:24] = 8'h0;
  
end

endfunction

endmodule
