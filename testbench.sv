`include "ram_if.sv"
`include "ram.sv"
`include "ram_pkg.sv"
`include "ram_assertion.svh"

module testbench;
  import uvm_pkg::*;
  import ram_pkg::*;
  
  reg clk,n_rst;
  ram_if        ram_vif(clk,n_rst);

  //=================================================
  //Generate clock and Reset
  //=================================================
  initial
  begin
    generate_reset(); 
    clk   = 0;
    forever #5 clk = ~clk;
  end
  
  //=================================================
  //Generate Reset for Reset test
  //=================================================
  always @(ram_vif.reset)
  begin
    generate_reset(); 
  end
  
  task generate_reset();
    n_rst = 0;
    #10;
    n_rst = 1;
  endtask
  
  //=================================================
  //Set Virtual Interface and call run_test
  //=================================================
  initial begin
    uvm_config_db#( virtual ram_if )::set(null, "*", "ram_if" , ram_vif); 
    run_test();
  end
  //=================================================
  //DUT Instance
  //=================================================
  ram ram_dut(ram_vif);

  bind ram ram_assertions ram_assertions_inst(.*);
  // Dump waves
  initial begin
    $dumpfile("dump.vcd");
    $dumpvars(0, testbench);
  end
 
endmodule: testbench
// Code your testbench here
// or browse Examples
