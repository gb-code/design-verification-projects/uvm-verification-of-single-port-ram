class ram_agent extends uvm_agent;
  `uvm_component_utils(ram_agent)
 
  ram_sequencer ram_seqr;
  ram_driver    ram_drvr;
  ram_monitor   ram_mon;
  uvm_analysis_port #(ram_transaction) ram_ap;  

  
  function new(string name, uvm_component parent);
     super.new(name, parent);
  endfunction: new
 
  function void build_phase(uvm_phase phase);
    super.build_phase(phase);
 
    ram_seqr = ram_sequencer::type_id::create(.name("ram_seqr"), .parent(this));
    ram_drvr = ram_driver   ::type_id::create(.name("ram_drvr"), .parent(this));
    ram_mon  = ram_monitor  ::type_id::create(.name("ram_mon"),  .parent(this));
    ram_ap = new(.name("ram_ap"),.parent(this));
  endfunction: build_phase
 
  function void connect_phase(uvm_phase phase);
    super.connect_phase(phase);
    ram_drvr.seq_item_port.connect(ram_seqr.seq_item_export);
    ram_mon.ram_ap.connect(this.ram_ap);
  endfunction: connect_phase
endclass: ram_agent
