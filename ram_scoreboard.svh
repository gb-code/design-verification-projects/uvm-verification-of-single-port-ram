`uvm_analysis_imp_decl( _mon )
class ram_scoreboard extends uvm_scoreboard;
   bit [31:0] RAM_EXP_ARRAY[int];
   virtual ram_if ram_vif;
  
   uvm_analysis_imp_mon #(ram_transaction, ram_scoreboard) aport_mon;
  
  `uvm_component_utils(ram_scoreboard)
  
  uvm_tlm_fifo #(ram_transaction) actfifo; 
    
  function new(string name, uvm_component parent);
      super.new(name, parent);
  endfunction: new
  
  function void build_phase(uvm_phase phase);
    super.build_phase(phase);
    aport_mon = new("aport_mon",this);
    actfifo   = new("actfifo",this);
    assert(uvm_config_db#(virtual ram_if):: get(.cntxt(this), .inst_name(""), .field_name("ram_if"),.value(ram_vif)))
    else  `uvm_fatal("NORMAL", "ram_intf is not available in config_db in driver\n");
  endfunction: build_phase
 
  function void connect_phase(uvm_phase phase);
    super.connect_phase(phase);
  endfunction: connect_phase
 
   
  function void write_mon(ram_transaction mon_tr); 
    void'(actfifo.try_put(mon_tr));  
  endfunction :write_mon 
  
  
  task run_phase(uvm_phase phase);
    ram_transaction exp_tr, out_tr;
    int ram_address_mask,ram_mask;
    forever 
    begin   
      fork
      begin  
        wait(ram_vif.n_rst == 0);
        RAM_EXP_ARRAY.delete;
        wait(ram_vif.n_rst == 1);
      end
      begin 
        begin
          actfifo.get(out_tr);
          if(out_tr.en == READ && out_tr.ready == 1)
          begin  
            if(RAM_EXP_ARRAY[out_tr.address] != out_tr.data)
            begin  
              `uvm_error(get_type_name(),$sformatf("ERROR :- Data for Read Request Failed for address = %x; Actual data = %x and Expected data =%x ",out_tr.address,out_tr.data,RAM_EXP_ARRAY[out_tr.address]))
          
            end  
            else
            begin
              `uvm_info(get_type_name(),$sformatf("Data for Read Request Matched for address = %x and data = %x ",out_tr.address,out_tr.data),UVM_LOW)          
            end
          end
          if(out_tr.en == WRITE && out_tr.valid == 1)
          begin  
            ram_mask = 0;
            ram_address_mask = 0;        
            ram_address_mask = (out_tr.address[1:0] == 0 ? 32'hFFFFFFFF  : 
                            out_tr.address[1:0] == 1 ? 32'hFFFFFF00  :
                            out_tr.address[1:0] == 2 ? 32'hFFFF_0000 : 32'hFF00_0000);
        
            if(out_tr.mask[0] == 1)
              ram_mask[7:0] = 8'hFF;
            if(out_tr.mask[1] == 1)
              ram_mask[15:8] = 8'hFF;
            if(out_tr.mask[2] == 1)
              ram_mask[23:16] = 8'hFF;
            if(out_tr.mask[3] == 1)
              ram_mask[31:24] = 8'hFF;      
        
            RAM_EXP_ARRAY[out_tr.address] = out_tr.data & ram_mask & ram_address_mask;
            `uvm_info(get_type_name(),$sformatf("Data for WRITE Request is =%x for address = %x",out_tr.address,out_tr.data),UVM_LOW)
               
          end  //End of if statement 
          /*
               //no operation display taken care off
          if(out_tr.en == NO_OP1 && out_tr.valid ===1)
            begin
              `uvm_info(get_type_name(),$sformatf("with en = %x, no operation will happen",out_tr.en),UVM_LOW)
            end
          */
      end  //End of else part
            end
            join_any  
          end //end of forever
        endtask :run_phase

      endclass





