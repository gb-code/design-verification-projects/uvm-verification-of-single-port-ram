class random_write_followed_by_read_test extends ram_base_test;
  `uvm_component_utils( random_write_followed_by_read_test )
   
  function new( string name="random_write_followed_by_read_test", uvm_component parent );
    super.new( name, parent );
  endfunction: new
 
  function void build_phase( uvm_phase phase );
     super.build_phase( phase );   
  endfunction: build_phase

  virtual function void end_of_elaboration_phase(uvm_phase phase);
    super.end_of_elaboration_phase(phase);
   /*
   Making fixed sequence, by overiding the base sequence!
   */
    ram_base_sequence::type_id::set_type_override(ram_read_mem_samelocation_sequence::get_type());
    
  endfunction : end_of_elaboration_phase  

endclass: random_write_followed_by_read_test
