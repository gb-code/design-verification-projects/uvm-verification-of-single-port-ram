class ram_base_sequence extends uvm_sequence#(ram_transaction);
  int number_of_transactions;
  
  `uvm_object_utils(ram_base_sequence)
 
  function new(string name = "");
     super.new(name);
  endfunction: new
 
  task body();
     ram_transaction ram_tx;
     ram_tx = ram_transaction::type_id::create(.name("ram_tx"), .contxt(get_full_name()));
     
    if(!$value$plusargs("no_of_trans=%d",number_of_transactions))
       number_of_transactions = 10;
    
     repeat(number_of_transactions)
     begin
       start_item(ram_tx);
       assert(ram_tx.randomize());
       finish_item(ram_tx);
     end
  endtask: body
endclass: ram_base_sequence

    /*
    Test Plan -4
    Function: Write all memory location
    Name of Sequence:	ram_write_mem_sequence
    */
class ram_write_mem_sequence extends ram_base_sequence;
  rand bit [9:0] address[$];
  
  `uvm_object_utils(ram_write_mem_sequence)
 
  function new(string name = "");
     super.new(name);
  endfunction: new
 
  task body();
     ram_transaction ram_tx;
     ram_tx = ram_transaction::type_id::create(.name("ram_tx"), .contxt(get_full_name()));
     
    if(!$value$plusargs("no_of_trans=%d",number_of_transactions))
       number_of_transactions = 4;
    
     repeat(number_of_transactions)
     begin
       start_item(ram_tx);
       assert(ram_tx.randomize() with {address <= number_of_transactions; valid == 1; en == 2'b01;})          
       finish_item(ram_tx);
     end
  endtask: body
endclass : ram_write_mem_sequence

	/*
    Test Plan -5
    Function: Read all memory location
    Name of Sequence:	ram_read_mem_sequence_all
    */
class ram_read_mem_sequence_all extends ram_base_sequence;
  rand  bit [9:0] address_array[];
  rand  int       data;
  bit       [9:0] idx;
  //simple counter to increment memory address sequentially
  integer count_address=0;
  
  
  `uvm_object_utils(ram_read_mem_sequence_all)
 
  function new(string name = "");
     super.new(name);
  endfunction: new
 
  task body();
    ram_transaction ram_wr_tx;
    ram_transaction ram_rd_tx;
    ram_wr_tx = ram_transaction::type_id::create(.name("ram_wr_tx"), .contxt(get_full_name()));
    ram_rd_tx = ram_transaction::type_id::create(.name("ram_rd_tx"), .contxt(get_full_name()));
    
    if(!$value$plusargs("no_of_trans=%d",number_of_transactions))
       number_of_transactions = 12;
 
    address_array = new[number_of_transactions];
    repeat(number_of_transactions)
    begin
      start_item(ram_wr_tx);   
      
      //just a simple counter keeping track of incremental address
      
      assert(ram_wr_tx.randomize() with {address == count_address;valid == 1; en == 2'b01;})
        address_array[idx] = count_address;
      
      //incrementing the counter and id now
      idx++;count_address++;
         finish_item(ram_wr_tx);
     end
    // initializing the id to zero after all write done for read transactions 
    idx=0;
     repeat(number_of_transactions)
     begin
       start_item(ram_rd_tx);
       assert(ram_rd_tx.randomize() with {address == address_array[idx]; en == 2'b10;})         
       idx++;  
       finish_item(ram_rd_tx);
     end
    
  endtask: body
  
  
endclass : ram_read_mem_sequence_all


   /*
    Test Plan -6
    Function: Reading after randomly generating write    
    Name of Sequence:											ram_read_mem_previouslygenerated_sequence
    */
class ram_read_mem_previouslygenerated_sequence extends ram_base_sequence;
  rand  bit [9:0] address_array[];
  rand  int       data;
  bit       [9:0] idx;
  randc bit [9:0] unique_address;
  
  
  constraint unique_address_c { unique_address % 4 ==0;}
  `uvm_object_utils(ram_read_mem_previouslygenerated_sequence)
 
  function new(string name = "");
     super.new(name);
  endfunction: new
 
  task body();
    ram_transaction ram_wr_tx;
    ram_transaction ram_rd_tx;
    ram_wr_tx = ram_transaction::type_id::create(.name("ram_wr_tx"), .contxt(get_full_name()));
    ram_rd_tx = ram_transaction::type_id::create(.name("ram_rd_tx"), .contxt(get_full_name()));
    
    if(!$value$plusargs("no_of_trans=%d",number_of_transactions))
       number_of_transactions = 12;
 
    address_array = new[number_of_transactions];
    repeat(number_of_transactions)
    begin
      start_item(ram_wr_tx);   
      void'(this.randomize(unique_address));
      assert(ram_wr_tx.randomize() with {address == unique_address;valid == 1; en == 2'b01;})
        address_array[idx] = unique_address;  
            
      finish_item(ram_wr_tx);    
     end
	// initializing the id to zero after all write done for read transactions
     idx=0;
     repeat(number_of_transactions)
     begin
       start_item(ram_rd_tx);
       assert(ram_rd_tx.randomize() with {address == address_array[idx]; en == 2'b10;})         
       idx++;  
       finish_item(ram_rd_tx);
     end

  endtask: body
  
endclass : ram_read_mem_previouslygenerated_sequence

	/*
    Test Plan -7
    Function: Writing and Reading at the same time 
    at random location
    Name of Sequence:	ram_read_mem_samelocation_sequence
    */
class ram_read_mem_samelocation_sequence extends ram_base_sequence;
  rand  bit [9:0] address_array[];
  rand  int       data;
  bit       [9:0] idx;
  randc bit [9:0] unique_address;
  
  
  constraint unique_address_c { unique_address % 4 ==0;}
  `uvm_object_utils(ram_read_mem_samelocation_sequence)
 
  function new(string name = "");
     super.new(name);
  endfunction: new
 
  task body();
    ram_transaction ram_wr_tx;
    ram_transaction ram_rd_tx;
    ram_wr_tx = ram_transaction::type_id::create(.name("ram_wr_tx"), .contxt(get_full_name()));
    ram_rd_tx = ram_transaction::type_id::create(.name("ram_rd_tx"), .contxt(get_full_name()));
    
    if(!$value$plusargs("no_of_trans=%d",number_of_transactions))
       number_of_transactions = 12;
 
    address_array = new[number_of_transactions];
    repeat(number_of_transactions)
    begin
      start_item(ram_wr_tx);   
      void'(this.randomize(unique_address));
      assert(ram_wr_tx.randomize() with {address == unique_address;valid == 1; en == 2'b01;})
        address_array[idx] = unique_address;    
      finish_item(ram_wr_tx);
       
      //starting reading transaction immediately
      
      start_item(ram_rd_tx);
       assert(ram_rd_tx.randomize() with {address == 				address_array[idx]; en == 2'b10;})         
      finish_item(ram_rd_tx);   
        //now increasing array id for another transaction
         idx++;
     end
    
  endtask: body
  
endclass : ram_read_mem_samelocation_sequence

    
    
/*
    Test Plan -12
    Function: No RAM operation
    Name of Sequence:	ram_no_operation_sequence
    */
class ram_no_operation_sequence extends ram_base_sequence;
  rand bit [9:0] address[$];
  
  `uvm_object_utils(ram_no_operation_sequence)
 
  function new(string name = "");
     super.new(name);
  endfunction: new
 
  task body();
     ram_transaction ram_tx;
     ram_tx = ram_transaction::type_id::create(.name("ram_tx"), .contxt(get_full_name()));
     
    if(!$value$plusargs("no_of_trans=%d",number_of_transactions))
       number_of_transactions = 4;
    
     repeat(number_of_transactions)
     begin
       start_item(ram_tx);
       assert(ram_tx.randomize() with {address <= number_of_transactions; valid == 1; en==2'b00 || en==2'b11;})          
       finish_item(ram_tx);
     end
  endtask: body
endclass : ram_no_operation_sequence

    /*
    Test Plan -3
    Function: Reset Sequence
    Name of Sequence:	ram_reset_sequence
    */

    class ram_reset_sequence extends ram_base_sequence;
     bit [15:0] first_last_address;
     virtual ram_if  ram_vif;

    `uvm_object_utils(ram_reset_sequence)
 
    function new(string name = "");
      super.new(name);
      if(!uvm_config_db#(virtual ram_if):: get(uvm_root::get(), "", "ram_if", ram_vif))
        `uvm_fatal("NORMAL", "ram_intf is not available in config_db in sequence\n");
    endfunction: new
 
    task body();
     ram_transaction ram_tx;
     ram_tx = ram_transaction::type_id::create(.name("ram_tx"), .contxt(get_full_name()));
     
    if(!$value$plusargs("no_of_trans=%d",number_of_transactions))
       number_of_transactions = 2;
    
     repeat(number_of_transactions)
     begin
       start_item(ram_tx);
       assert(ram_tx.randomize() with {address == first_last_address; en == 2'b01;})         
       finish_item(ram_tx);   
       first_last_address = `RAM_SIZE - 1;
     end
     first_last_address = 0;
     ram_vif.reset = 1; 
      
     repeat(number_of_transactions)
     begin
       start_item(ram_tx);
       assert(ram_tx.randomize() with {address == first_last_address; en == 2'b10;})         
       finish_item(ram_tx);   
       first_last_address = `RAM_SIZE - 1;
     end
  endtask: body
endclass : ram_reset_sequence

