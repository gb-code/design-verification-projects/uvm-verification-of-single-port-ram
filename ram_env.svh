class ram_env extends uvm_env;
  //ram_env_cfg    ram_env_cfg_inst;
  ram_scoreboard   ram_scoreboard_inst;
  ram_agent        ram_agent_inst;
  
  `uvm_component_utils(ram_env)

  function new(string name = "ram_env", uvm_component parent);
    super.new(name,parent);
  endfunction : new

  function void build_phase(uvm_phase phase);
    super.build_phase(phase);
    ram_agent_inst  = ram_agent  ::type_id::create(.name("ram_agent_inst"),  .parent(this));
    ram_scoreboard_inst = ram_scoreboard :: type_id :: create(.name("ram_scoreboard_inst"), .parent(this));
  endfunction : build_phase

  function void connect_phase(uvm_phase phase);
    ram_agent_inst.ram_ap.connect(ram_scoreboard_inst.aport_mon);
  endfunction : connect_phase
endclass : ram_env
