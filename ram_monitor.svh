class ram_monitor extends uvm_monitor;
  virtual ram_if ram_vif;
  ram_transaction ram_tx;
  
  `uvm_component_utils(ram_monitor)
 
  uvm_analysis_port #(ram_transaction) ram_ap;  


  function new(string name="ram_monitor", uvm_component parent=null);
    super.new(name,parent);
  endfunction : new

  //---------------------------------------------------------------
  // Function Name            : build_phase 
  //---------------------------------------------------------------
  function void build_phase(uvm_phase phase);
    super.build_phase(phase);

    assert(uvm_config_db#(virtual ram_if):: get(.cntxt(this), .inst_name(""), .field_name("ram_if"),.value(ram_vif)))
    else  `uvm_fatal("NORMAL", "ram_intf is not available in config_db in monitor!\n");

    ram_ap = new(.name("ram_ap"),.parent(this));

  endfunction : build_phase

  //---------------------------------------------------------------
  // Function Name            : run_phase 
  //---------------------------------------------------------------
  task run_phase(uvm_phase phase);
    `uvm_info("TRACE", $sformatf("%m"), UVM_HIGH);
    forever 
    begin
      wait(ram_vif.n_rst == 1);
       //On First positive edge of clock- address, mask and en is available
      @(ram_vif.ram_mon_cb); 
      if(ram_vif.en == READ || ram_vif.en == WRITE)
      begin 
        ram_tx = ram_transaction::type_id::create("ram_tx", this);
        ram_tx.address   = ram_vif.address;
        ram_tx.mask      = ram_vif.mask;
        ram_tx.en        = ram_vif.en;

        //On next positive edge of clock cycle - read data/ready/valid is available
        @(ram_vif.ram_mon_cb);
        
        if(ram_tx.en == READ)
        begin
          ram_tx.data    = ram_vif.dout;
        end 
        else
        begin
          ram_tx.data    = ram_vif.din;
        end
        ram_tx.ready     = ram_vif.ready;
        ram_tx.valid     = ram_vif.valid;
   
        ram_ap.write(ram_tx);
        `uvm_info("Monitor Values: ",$sformatf("\n address = %0d",ram_tx.address), UVM_LOW)
      end  
      else
      begin
        `uvm_info(get_type_name(),$sformatf("Request is No operation = %x",ram_vif.en),UVM_DEBUG) 
      end
    end
  endtask : run_phase
endclass : ram_monitor

