class ram_base_test extends uvm_test;
  `uvm_component_utils(ram_base_test)
 
  ram_env          ram_env_inst;
  virtual   ram_if ram_vif;
 
  function new(string name="ram_base_test", uvm_component parent );
    super.new( name, parent );
  endfunction: new
 
  function void build_phase( uvm_phase phase );
     super.build_phase( phase );
 
  
     ram_env_inst = ram_env::type_id::create( .name( "ram_env_inst" ), .parent( this ) );
    
     assert(uvm_config_db#(virtual ram_if):: get(.cntxt(this), .inst_name(""), .field_name("ram_if"),.value(ram_vif)))
     else  `uvm_fatal("NORMAL", "ram_intf is not available in test TOP\n");
  endfunction: build_phase

  //================================
  //TEST PASS/FAIL information print
  //================================
  function void report_phase(uvm_phase phase);
    uvm_report_server svr;
    super.report_phase(phase);
    svr = uvm_report_server::get_server();
    assert (svr.get_severity_count(UVM_FATAL) == 0  &&  svr.get_severity_count(UVM_ERROR) == 0)
    begin
      `uvm_info(get_type_name(),"TEST PASSED",UVM_LOW);
    end
    else
      `uvm_info(get_type_name(),"TEST FAILED",UVM_LOW);
  endfunction: report_phase

  task run_phase(uvm_phase phase);
    ram_base_sequence base_seq;
     
    `uvm_info(get_type_name(), "Test has started", UVM_LOW)
    base_seq =  ram_base_sequence::type_id::create( .name( "base_seq" ) );
    assert(base_seq.randomize());
  
    phase.raise_objection(this);
    base_seq.start(ram_env_inst.ram_agent_inst.ram_seqr);
    phase.drop_objection(this);
    phase.phase_done.set_drain_time(this, 20ns);
    `uvm_info(get_type_name(), "Test has finished", UVM_LOW)
  endtask:run_phase
endclass: ram_base_test
