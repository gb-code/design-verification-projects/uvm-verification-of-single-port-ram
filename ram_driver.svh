class ram_driver extends uvm_driver#(ram_transaction);

  `uvm_component_utils(ram_driver)

   virtual ram_if ram_vif;

   function new(string name="ram_driver", uvm_component parent);
     super.new(name, parent);
   endfunction : new

   //---------------------------------------------------------------
   // Function Name            : build_phase 
   //---------------------------------------------------------------
   function void build_phase(uvm_phase phase);
     super.build_phase(phase);
     assert(uvm_config_db#(virtual ram_if):: get(.cntxt(this), .inst_name(""), .field_name("ram_if"),.value(ram_vif)))
     else  `uvm_fatal("NORMAL", "ram_intf is not available in config_db in driver\n");
   endfunction : build_phase


   //---------------------------------------------------------------
   // Task Name                : run_phase 
   //---------------------------------------------------------------
   task run_phase(uvm_phase phase);
     ram_transaction ram_tx;
     
     forever
     begin
       //Drive Data on every positive edge of clock
       @ram_vif.ram_drv_cb; 
       
       //Get New transaction - Sequence Item
       seq_item_port.get_next_item(ram_tx); 

       //For Write Requests drive data and valid, 
       //For Read Requests interface will hold previous values
       if(ram_tx.en == WRITE)
       begin
         ram_vif.din   = ram_tx.data;
       end
       
       ram_vif.mask    = ram_tx.mask;
       ram_vif.address = ram_tx.address;
       ram_vif.en      = ram_tx.en;
       ram_vif.valid   = 0;//Drive Valid = 0 for the first cycle
      
       //The requirement is to drive en = WRITE/READ for only one clock cycle 
       //Valid goes high one clock cycle later
       //Wait for next positive edge clock cycle and drive en = 0 and valid
       @ram_vif.ram_drv_cb; 

       ram_vif.en      = ram_tx.no_op;
       ram_vif.valid   = ram_tx.valid;
           
       seq_item_port.item_done(); 
     end
   endtask : run_phase
endclass : ram_driver
